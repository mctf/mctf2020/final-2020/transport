import rpc
import db

from user import get_user
import auth

@rpc('driver.register')
async def register(attrs, _ctx):
    """ Список публичных таксистов """
    await auth.can('driver.register', _ctx)
    with db.db_session:
        user = await get_user(_ctx)
        if user.driver:
            raise Exception('Пользователь уже зарегистрирован как водитель')
        db.Driver(user=user, **attrs)
        user.role = db.Role.get(name='Driver')

@rpc('driver.get')
async def get(_ctx):
    """ Получить запись таксиста (Для таксиста) """
    await auth.can('driver.get', _ctx)
    with db.db_session:
        user = await get_user(_ctx)
        return user.driver.to_dict()

@rpc('driver.set')
async def set_(attrs, _ctx):
    """ Изменить запись таксиста (Для таксиста) """
    await auth.can('driver.set', _ctx)
    with db.db_session:
        user = await get_user(_ctx)
        user.driver.set(**attrs)

@rpc('driver.random')
async def random(filters, _ctx):
    """ Случайный таксист """
    await auth.can('driver.random', _ctx)
    with db.db_session:
        driver = db.Driver.select().filter(**filters).random(1)
        if not len(driver):
            raise ValueError('Водитель с заданными параметрами не найден')
        return driver[0].public_dict()

@rpc('driver.list')
async def list_driver(filters, _ctx):
    """ Список публичных таксистов """
    await auth.can('driver.list', _ctx)
    with db.db_session:
        return [driver.public_dict() for driver in db.Driver.select().filter(**{'is_private':False, **filters})]