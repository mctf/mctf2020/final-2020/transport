import rpc
import db

import auth
from user import get_user

from math import cos, asin, sqrt, pi
def calc_km(lat1: float, lon1: float, lat2: float, lon2: float) -> float:
    """ Расстояние между географическими координатами в километрах """
    p = pi/180
    a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p) * cos(lat2*p) * (1-cos((lon2-lon1)*p))/2
    return 12742 * asin(sqrt(a))

@rpc('order.create')
async def create_order(driver_id: int, to: list, prepaid: bool, _ctx):
    """ Вызвать такси """
    await auth.can('order.create', _ctx)
    with db.db_session:
        user = await get_user(_ctx)
        driver = db.Driver.get(id=driver_id)
        if driver is None:
            raise ValueError('Водитель с заданными параметрами не найден')
        price = driver.tarriff_base + driver.tarriff_per_km * calc_km(user.profile.lat, user.profile.lon, to[0], to[1])
        if prepaid is True:
            if user.profile.balance < price:
                raise ValueError('У вас недостаточно средств')
            user.profile.balance -= price
        order = db.Order(user=user, driver=driver, prepaid=prepaid, from_lat=user.profile.lat, from_lon=user.profile.lon, to_lat=to[0], to_lon=to[1], price=price)
        if prepaid:
            order.gift = driver.prepaid_comment
        # объекты создадутся только после закрытия транзакции, так что для получения id надо пнуть сущность
        order.flush()
        return order.id

@rpc('order.get')
async def get(order_id, _ctx):
    """ """
    await auth.can('order.get', _ctx)
    with db.db_session:
        if res := db.Order.get(id=order_id):
            return res.to_dict()

@rpc('order.list')
async def list_orders(_ctx):
    """ Список моих заказов """
    await auth.can('order.list', _ctx)
    with db.db_session:
        user = await get_user(_ctx)
        if user.driver:
            return [order.to_dict() for order in db.Order.select().filter(driver=user.driver)]
        return [order.to_dict() for order in db.Order.select().filter(user=user)]