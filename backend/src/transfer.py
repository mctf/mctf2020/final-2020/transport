import rpc
import db

from user import get_user

import auth

@rpc('transfer')
async def transfer(to, count, _ctx):
    await auth.can('transfer', _ctx)
    with db.db_session:
        user = await get_user(_ctx)
        if user.profile.balance < count:
            raise ValueError('Недостаточно средств на вашем балансе')
        to = db.User.get(name=to)
        if to is None:
            raise NameError('Пользователь не найден')
        to.profile.balance += count
        user.profile.balance -= count