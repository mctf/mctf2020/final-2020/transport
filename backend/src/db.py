from datetime import datetime
from uuid import UUID, uuid4
from pony.orm import *

db = Database()
db.bind('sqlite', '/data/db', create_db=True)

def get_or_create(cls, **params):
    noniterableparams = dict(filter(lambda x: not isinstance(x[1], list), params.items()))
    return cls.get(**noniterableparams) or cls(**params)

class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    password = Required(str)
    sessions = Set('Session')
    role = Required('Role')
    profile = Optional('Profile')
    driver = Optional('Driver')
    orders = Set('Order')


class Session(db.Entity):
    id = PrimaryKey(UUID, default=uuid4)
    user = Required(User)


class Role(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    users = Set(User)
    permissions = Set('Permission')
    extends = Set('Role', reverse='successors')
    successors = Set('Role', reverse='extends')

class Permission(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    role = Required(Role)

class Profile(db.Entity):
    id = PrimaryKey(int, auto=True)
    user = Required(User)
    name = Required(str)
    passport = Required(str)
    insurance = Required(str)
    chip = Required(str)
    lat = Optional(float, default=0)
    lon = Optional(float, default=0)
    credit_cards = Set('CreditCard')
    balance = Required(float, default=0)


class CreditCard(db.Entity):
    id = PrimaryKey(int, auto=True)
    is_suer = Required(str)
    card_number = Required(str)
    expiration_month = Required(str)
    expiration_year = Required(str)
    cvv = Required(str)
    name = Required(str)
    address = Optional(str)
    zipcode = Optional(str)
    profile = Required(Profile)


class Driver(db.Entity):
    id = PrimaryKey(int, auto=True)
    car = Required(str)
    is_premium = Optional(bool, default=False)
    passangers = Optional(int, default=4)
    tarriff_base = Optional(int, default=200)
    tarriff_per_km = Optional(int, default=15)
    lat = Optional(float, default=0)
    lon = Optional(float, default=0)
    description = Optional(str)
    prepaid_comment = Optional(str)
    user = Required(User)
    orders = Set('Order')
    is_private = Optional(bool, default=True)

    def public_dict(self):
        return dict(id=self.id, tarriff_base=self.tarriff_base, tarriff_per_km=self.tarriff_per_km, name=self.user.profile.name, username=self.user.name, car=self.car, is_premium=self.is_premium, description=self.description.format(self=self), lat=self.lat, lon=self.lon)


class Order(db.Entity):
    id = PrimaryKey(int, auto=True)
    user = Required(User)
    driver = Required(Driver)
    prepaid = Optional(bool, default=True)
    from_lat = Optional(float)
    from_lon = Optional(float)
    to_lat = Optional(float)
    to_lon = Optional(float)
    price = Optional(float)
    gift = Optional(str)


db.generate_mapping(create_tables=True)