import rpc
import db

import auth

@rpc('analytics.sql')
async def sql(sql: str, _ctx) -> None or Exception:
    """ Транзакции для аналитиков """
    await auth.can('analytics.sql', _ctx)
    with db.db_session:
        return db.db.execute(sql).fetchall()