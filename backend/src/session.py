import rpc
import db

import uuid

def get_session(session: str) -> db.Session:
    """ Получение записи сессии """
    return db.Session.get(id=session)

@rpc('session.check')
async def check(session: str, _ctx) -> bool:
    """ Проверка наличия привязанного к сессии пользователя """
    with db.db_session:
        if s := get_session(session):
            await rpc.call('sessions._set', _ctx.caller, 'user', s.user.name)
            await rpc.call('sessions._set', _ctx.caller, 'session', session)
            return True
    return False
