import rpc
import db

from user import get_user
from session import get_session

with db.db_session:
    UserRole = db.get_or_create(db.Role, name='User')
    db.get_or_create(db.Permission, name='auth.can', role=UserRole)

    db.get_or_create(db.Permission, name='profile.set', role=UserRole)
    db.get_or_create(db.Permission, name='profile.get', role=UserRole)

    db.get_or_create(db.Permission, name='transfer', role=UserRole)

    db.get_or_create(db.Permission, name='referal.sign', role=UserRole)
    db.get_or_create(db.Permission, name='referal.create', role=UserRole)
    db.get_or_create(db.Permission, name='referal.get', role=UserRole)

    db.get_or_create(db.Permission, name='order.create', role=UserRole)
    db.get_or_create(db.Permission, name='order.list', role=UserRole)
    db.get_or_create(db.Permission, name='order.get', role=UserRole)

    db.get_or_create(db.Permission, name='driver.random', role=UserRole)
    db.get_or_create(db.Permission, name='driver.list', role=UserRole)

    db.get_or_create(db.Permission, name='driver.register', role=UserRole)

    DriverRole = db.get_or_create(db.Role, name='Driver', extends=[UserRole])
    db.get_or_create(db.Permission, name='driver.get', role=DriverRole)
    db.get_or_create(db.Permission, name='driver.set', role=DriverRole)

    AnalyticRole = db.get_or_create(db.Role, name='Analytic', extends=[UserRole])
    db.get_or_create(db.Permission, name='analytics.sql', role=AnalyticRole)

@rpc('auth.can')
async def _can(permission: str, session=None, _ctx={}) -> None or Exception:
    """ проверка разрешений """
    with db.db_session:
        if session is not None:
            user = get_session(session).user
        else:
            user = await get_user(_ctx)
        checked = set()
        stack = [user.role]
        while len(stack):
            role = stack[-1]
            if db.Permission.select().filter(name=permission, role=role).exists():
                return True
            checked.add(role)
            for role_ in role.extends:
                if role_ not in checked:
                    stack.append(role_)
                    break
            if role == stack[-1]:
                stack.pop()
        raise Exception(f'"{permission}" denied for "{user.role.name}"')

async def can(permission: str, _ctx):
    session = await rpc.call('sessions._get', _ctx.caller, 'session')
    if session is None:
        raise Exception('Permission denied')
    await rpc.call('auth.can', permission, session)
