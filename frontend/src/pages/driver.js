import { z, page, Ref } from '../2ombular';
import * as auth from '../auth';
import RPC from '../rpc';

let modal, error;

const Input = (Val, p) => z._input.c1.b4({
    oninput(e) {
        error = '';
        Val(e.target.value);
        page.update()
    }, value: Val, ...p
});
const Line = (icon, title, val) => z.flex.sp3(z.v.flex.jc.ac({style: '--ggs: 1.7; width: 30px'}, z._i.c2[icon]), z.v({style: 'margin-left: 30px'}, z.c2.f2(title), z.c1.f3(val)))

const Check = (val, text) => z.v.cp.flex.ac.usn({ onclick(e) { val(!val()); page.update(); }}, _=>z.c2['gg-check-'+ (val() ? 'r' : 'n')].ib(), z.v({style: 'margin: .2em'}), text);

const numf = new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'RUB' })

let data = {}, c = {};
const Main = _=> z.main(
    z.v({onclick(e) {
        modal.style.display = 'flex';
    }},
    Line('gg-bmw', 'Наименование модели', data.car || '<Пусто>'),
    Line('gg-user', 'Количество пассажиров', data.passangers),
    Line('gg-awards', 'Бизнес класс', z.c2['gg-check-'+ (data.is_premium ? 'r' : 'n')].ib()),
    Line('gg-chevron-down', 'Цена за вызов', numf.format(data.tarriff_base)),
    Line('gg-chevron-double-right', 'Цена за километр', numf.format(data.tarriff_per_km)),
    Line('gg-info', 'Описание', data.description || '<Пусто>'),
    Line('gg-gift', 'Комментарий к предоплаченным вызовам', data.prepaid_comment  || '<Пусто>')),
    
    z.modal__overlay(
        { key: 'driver-modal', on$created(ev) {
            modal = ev.target;
            window.addEventListener('click', e => {
                if (e.target == modal) {
                    modal.style.display = 'none';
                }
            })
        } },
        z.modal__container.f2(
            z.modal__header('Стать партнером'),
            z.modal__content(
                z.f3('Автомобиль'),
                z.sp1(Input(Ref(c, 'car'), {placeholder: 'Наименование модели', required: true})),
                z.sp1(Input(Ref(c, 'passangers'), {placeholder: 'Количество пассажиров', type: 'number', min: 1})),
                z.sp1(Check(Ref(c, 'is_premium'), 'Бизнес класс')),
                z.sp2.f3('Цена'),
                z.sp1(Input(Ref(c, 'tarriff_base'), {placeholder: 'Цена за вызов', type: 'number', min: 50})),
                z.sp1(Input(Ref(c, 'tarriff_per_km'), {placeholder: 'Цена за километр', type: 'number', min: 5})),
                z.sp2.f3('Дополнительные сведения'),
                z.sp1(Input(Ref(c, 'description'), {placeholder: 'Описание'})),
                z.sp1(Input(Ref(c, 'prepaid_comment'), {placeholder: 'Комментарий к предоплаченным вызовам'})),
                _=>z.sp2.c4(error)
            ),
            z.modal__footer(
                z.flex(
                    z.modal__btn.primary({
                        onclick(e) {
                            RPC.session.call('driver.set', [c])
                            .then(res => {
                                Object.assign(data, c);
                                modal.style.display = 'none';
                            })
                            .catch(e => {
                                error = e.args[0];
                            })
                            .finally(page.update);
                        }
                    }, 'Сохранить'),
                    z.modal__btn({style:'margin-left:15px', onclick(e) {
                        c = {};
                        modal.style.display = 'none';
                        page.update();
                    }}, 'Закрыть'))
            )
        )
    )
);

const get_driver = user =>
user.role === 2 &&
RPC.session.call('driver.get')
    .then(res => {
        data = res;
        Object.assign(c, data);
    })
    .catch(console.error)
    .finally(page.update)

auth.e.on('user.get', get_driver);

export default Main;