import { z, page, Ref } from '../2ombular';
import * as auth from '../auth';
import RPC from '../rpc';

const Input = (Val, p) => z._input.c1.b4({
    oninput(e) {
        error = '';
        Val(e.target.value);
        page.update()
    }, value: Val, ...p
});

let data = {}, error;
const Main = _=> z.main.f3(
    z.f5('Перевод средств другому пользователю'),
    z.sp2(),
    Input(Ref(data, 'user'), {placeholder: 'Имя пользователя', required: true}),
    z.sp1(),
    Input(Ref(data, 'count'), {placeholder: 'Количество', type: 'number', min: 0, required: true}),
    _=>z.sp2.c4(error),
    z.sp2(),
    z.modal__btn.primary({
        onclick(e) {
            let c = true;
            document.querySelectorAll('input').forEach(i => {
                if (!i.checkValidity()) {
                    i.reportValidity();
                    c = false;
                }
            });
            if (c) {
                RPC.session.call('transfer', [data.user, Number(data.count)])
                .then(res => {
                    error = 'Перевод выполнен';
                    auth.e.emit('session.check', true);
                })
                .catch(e => {
                    error = e.args[0];
                })
                .finally(page.update);
            }
        }
    }, 'Отправить'),
);

export default Main;