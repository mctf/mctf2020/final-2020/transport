import { z, page, Ref } from '../2ombular';
import * as auth from '../auth';
import RPC from '../rpc';

import L from 'leaflet';

const Titled = (name, c) => z.v(z.f2.c2(name), c)

const numf = new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'RUB' })

let data = [], error, d = {}, modal, map, marker;
const Main = _=> z.main.f3(
    z.f5('Выберите водителя'),
    z.sp1(z._a.c4.f2.cp({
        onclick() {
            RPC.session.call('driver.random', [{}])
            .then(res => {
                d = res;
                page.update().then(_ => {
                    modal.style.display = 'flex'
                });
            })
            .catch(console.error)
            .finally(page.update)
        }
    }, 'Мне все равно, давайте любого')),
    data.map(i => z.sp3.cp(
        {
            onclick(e) {
                d = i;
                page.update().then(_ => {
                    modal.style.display = 'flex'
                });
            }
        },
        Titled('Имя водителя', i.name),
        z.flex.sp1(
            Titled('Модель автомобиля', i.car),
            z.v({style: 'margin-left: 30px'}),
            Titled('Бизнес класс', z.c2['gg-check-'+ (i.is_premium ? 'r' : 'n')].ib())
        ),
        z.flex.sp1(
            Titled('Цена за вызов', numf.format(i.tarriff_base)),
            z.v({style: 'margin-left: 30px'}),
            Titled('Цена за км', numf.format(i.tarriff_per_km))
        ),
        z.sp1(),
        Titled('Описание', i.description),
    )),
    z.modal__overlay(
        { key: 'search-modal', on$created(ev) {
            modal = ev.target;
            window.addEventListener('click', e => {
                if (e.target == modal) {
                    ev.target.style.display = 'none';
                }
            })
        } },
        z.modal__container.f2(
            z.modal__header('Подтвердите выбор'),
            _ => z.modal__content(
                Titled('Имя водителя', d.name),
                z.flex.sp1(
                    Titled('Модель автомобиля', d.car),
                    z.v({style: 'margin-left: 30px'}),
                    Titled('Бизнес класс', z.c2['gg-check-'+ (d.is_premium ? 'r' : 'n')].ib())
                ),
                z.flex.sp1(
                    Titled('Цена за вызов', numf.format(d.tarriff_base)),
                    z.v({style: 'margin-left: 30px'}),
                    Titled('Цена за км', numf.format(d.tarriff_per_km))
                ),
                z.sp1(),
                Titled('Описание', d.description),
                z.sp3('Выберите место назначения'),
                z.v({
                    style: 'width: 100%; height: 300px;',
                    on$created(e) {
                        map = L.map(e.target).setView([55.7558, 37.6173], 13);
                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
                        map.on('click', function(e) {
                            if(marker) map.removeLayer(marker);
                            marker = L.marker(e.latlng).addTo(map);
                        });
                    }
                }),
                z.sp2.c4(error)
            ),
            z.modal__footer(
                z.flex(
                    z.modal__btn.primary({
                        onclick(e) {
                            if (!marker) {
                                error = 'Выберите место назначения';
                                page.update();
                            } else
                                RPC.session.call('order.create', [d.id, [marker._latlng.lat, marker._latlng.lng], true])
                                .then(res => {
                                    modal.style.display = 'none';
                                    auth.e.emit('session.check', true);
                                })
                                .catch(e => {
                                    error = e.args[0];
                                })
                                .finally(page.update);
                        }
                    }, 'Вызвать'),
                    z.modal__btn({style:'margin-left:15px', onclick(e) {
                        modal.style.display = 'none';
                        page.update();
                    }}, 'Отмена'))
            )
        )
    )
);

const get_list = res =>
res &&
RPC.session.call('driver.list', [{}])
    .then(res => {
        data = res;
    })
    .catch(console.error)
    .finally(page.update)

auth.e.on('session.check', get_list);

export default Main;