import { z, page } from '../2ombular';
import * as auth from '../auth';
import Router from '../router';

import profile from './profile';
import driver from './driver';
import support from './support';
import order_list from './order_list';
import search_driver from './search_driver';
import transfer from './transfer';

let h, m, e, cur;

function calcTranslate() {
    let c3 = m.querySelector('.c3');
    if (c3) m.style.transform = `translateY(${-c3.offsetTop+30}px)`;
}

const Item = (name, onclick) => z.f5.sp1({class: _=>({c3: name == cur, c5: name != cur}), onclick}, name);

const Header = z.v.b3({style:'padding: 30px; width: 100%; height: 130px; transition: height .3s;', on$created(e) { h = e.target }},
    z.flex(
        z.flex.jc.cp.ac({style: 'width: 30px; height: 30px;', onclick() {
            if (!e) {
                h.style.height = m.clientHeight + 80 + 'px';
                m.style.transform = 'translateY(0px)';
                m.classList.add('open')
            } else {
                h.style.height = '130px';
                calcTranslate();
                m.classList.remove('open');
            }
            e = !e;
        }}, z._i.c3['gg-menu']),
        z.v({style: 'margin-left: 30px'}),
        z.v({
            on$created(ev) {
                e = false;
                m = ev.target;
                setTimeout(_ => {
                    ev.target.style.transition = 'transform 0s';
                    calcTranslate();
                    setTimeout(_=>ev.target.style.transition = 'transform .3s', 100);
                }, 1);
        }},
            Item('Выйти', auth.logout),
            _ => (auth.user ? auth.user.role === 1 : false) ? Item('Вызов такси', _=>Router.navigate('/search')) : '',
            Item('Переводы другим пользователям', _=>Router.navigate('/transfer')),
            Item('Мои поездки', _=>Router.navigate('/order_list')),
            _ => (auth.user ? auth.user.role === 2 : false) ? Item('Настройки таксиста', _=>Router.navigate('/driver')) : '',
            Item('Поддержка', _=>Router.navigate('/support')),
            Item('Профиль', _=>Router.navigate('/')))
    ))

auth.e.on('user.get', _ => {
    page.update().then(setTimeout(calcTranslate, 10));
})

const ov = c => z.ov.ovh.flex.col(Header, c);

Router.register('^/$', _ => {
    cur = 'Профиль';
    return ov(profile);
});

Router.register('^/driver$', _ => {
    cur = 'Настройки таксиста';
    return ov(driver);
});

Router.register('^/support$', _ => {
    cur = 'Поддержка';
    return ov(support);
});

Router.register('^/order_list$', _ => {
    cur = 'Мои поездки';
    return ov(order_list);
});

Router.register('^/search$', _ => {
    cur = 'Вызов такси';
    return ov(search_driver);
});

Router.register('^/transfer$', _ => {
    cur = 'Переводы другим пользователям';
    return ov(transfer);
})