import { z, page, Ref } from './2ombular';
import RPC from './rpc';
import EventEmitter from './events';
import Router from './router';

let logged, v, error, wait = true;
let data = {};

export const e = new EventEmitter();
export const user = {};

export function logout() {
    RPC.session.call('user.logout', [])
    .then(_ => {
        logged = false;
        e.emit('user.logout', {});
        Router.navigate('/');
    })
    .catch(console.error)
    .finally(page.update)
}

const Input = (Val, props) => z._input.c1.b1({
    oninput(e) {
        error = '';
        Val(e.target.value);
        page.update()
    }, value: Val, ...props
});

export const get_user = _=>
RPC.session.call('user.get')
    .then(res => {
        Object.assign(user, res);
        e.emit('user.get', res);
    })
    .catch(console.error)
    .finally(page.update);

e.on('session.check', res => {
    if (res) get_user()
})

RPC.on('open', _ => {
    if (localStorage.session)
        RPC.session.call('session.check', [localStorage.session])
        .then(res => {
            logged = res;
            e.emit('session.check', res);
        })
        .catch(console.error)
        .finally(page.update);
    wait = false;
    page.update();
})

function register() {
    if (data.password !== data.repeat) {
        Register.password.setCustomValidity("Пароли не совпадают");
        Register.password.reportValidity();
        setTimeout(_ => Register.password.setCustomValidity(""), 1000);
    } else {
        RPC.session.call('user.register', [data.username, data.password, {name: data.name, passport: data.passport, insurance: data.insurance, chip: data.chip}])
        .then(res => {
            localStorage.session = res;
            logged = true;
            v = Login;
            data = {}
            e.emit('session.check', res);
        })
        .catch(e => {
            error = e.args[0];
        })
        .finally(page.update)
    }
}

function login() {
    RPC.session.call('user.login', [data.username, data.password])
    .then(res => {
        localStorage.session = res;
        console.log(res)
        logged = true;
        data = {};
        e.emit('session.check', res);
    })
    .catch(e => {
        error = e.args[0];
    })
    .finally(page.update);
}

function forgot() {
    RPC.session.call('user.restore', [data])
    .then(res => {
        v = Password(res);
    }).catch(e => {
        error = e.args[0];
    })
    .finally(page.update)
}

const Password = p => _=>z.main.b3.s1({key: 'restore'},
z.c3.tc.f9.sp4('Ваш новый пароль'),
z.sp2(),
z.c3.tc.f5.w5(p),
z._button.sp2({onclick(e) { v=Login; error=''; data={}; page.update(); }}, 'Запомнил'),
)


const Register = _=>z.main.b3.s1({key: 'register'},
    z.c3.tc.f9.sp4('Регистрация'),
    z.sp2(),
    z._form({ onsubmit(e) { e.preventDefault(); register(); return false; }},
        z.sp1(Input(Ref(data, 'username'), { placeholder: 'Имя пользователя', required: true })),
        z.sp1(Input(Ref(data, 'password'), { placeholder: 'Пароль', required: true, on$created(e) { Register.password = e.target; }})),
        z.sp1(Input(Ref(data, 'repeat'), { placeholder: 'Повторение пароля', required: true })),

        z.sp2(Input(Ref(data, 'name'), { placeholder: 'ФИО', required: true })),
        z.sp1(Input(Ref(data, 'passport'), { placeholder: 'Номер паспорта', required: true })),
        z.sp1(Input(Ref(data, 'insurance'), { placeholder: 'СНИЛС', required: true })),
        z.sp1(Input(Ref(data, 'chip'), { placeholder: 'Номер чипа', required: true })),
        
        z._button.sp2('Зарегистрироваться'),
    ),
    z.sp1(z._a.c3.f1.cp({
        onclick(e) { v = Login; data = {}; error = ''; page.update(); }
    }, 'У меня уже есть аккаунт')),
    z.sp1(z.f1.c4(error))
)


const Forgot = _=>z.main.b3.s1({key: 'forgot'},
    z.c3.tc.f9.sp4('Восстановление пароля'),
    z.sp2(),
    z._form({ onsubmit(e) { e.preventDefault(); data = {}; error = ''; forgot(); return false; }},
        z.sp2(Input(Ref(data, 'name'), { placeholder: 'ФИО', required: true })),
        z.sp1(Input(Ref(data, 'passport'), { placeholder: 'Номер паспорта', required: true })),
        z.sp1(Input(Ref(data, 'chip'), { placeholder: 'Номер чипа', required: true })),
        
        z._button.sp2('Восстановить'),
    ),
    z.sp1(z._a.c3.f1.cp({
        onclick(e) { v = Login; data = {}; error = ''; page.update(); }
    }, 'Нет нужды поднимать архив')),
    z.sp1(z.f1.c4(error))
)

const Login =  _=>z.main.b3.s1({key: 'login'},
    z.c3.tc.f9.sp4('Вход'),
    z.sp2(),
    z._form({ onsubmit(e) { e.preventDefault(); login(); return false; }},
        z.sp1(Input(Ref(data, 'username'), { placeholder: 'Имя пользователя', required: true })),
        z.sp1(Input(Ref(data, 'password'), { placeholder: 'Пароль', required: true })),
        z._button.sp1('Войти'),
    ),
    z.sp1.flex(z._a.c3.f1.cp({
        onclick(e) { v = Register; data = {}; error = ''; page.update(); }
    }, 'Создать аккаунт'), z.s1(), z._a.c3.f1.cp({
        onclick(e) { v = Forgot; data = {}; error = ''; page.update(); }
    }, 'Забыл пароль')),
    z.sp1(z.f1.c4(error))
);

v = Login;

const Title = z.s2.b2.main.ovh(
    z.f9.car_text.tc.c3.sp4('Заказ такси'),
    z._img.car({src: 'http://pngimg.com/uploads/mercedes/mercedes_PNG80135.png'}));

export default (...c) => _ =>
    wait ? '' : logged ? z._(c)
    : z.ov.flex(window.innerWidth > 720 ? Title : '', v);
