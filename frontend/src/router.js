
import * as zombular from './2ombular';

const routes = {}

const clearSlashes = path => path.toString().replace(/\/$/, '').replace(/^\//, '');

function getFragment() {
    let fragment = '';
    fragment = clearSlashes(decodeURI(window.location.pathname + window.location.search));
    fragment = fragment.replace(/\?(.*)$/, '');
    return '/' + clearSlashes(fragment)
}

let current, page;
function render(frag, state={}) {
    Object.entries(routes).some(([route, cb]) => {
        const match = frag.match(route);
        if (match) {
            match.shift();
            page = cb.apply(state, match);
            return match;
        }
    });
}
window.onpopstate = function(event) {
    let frag = getFragment();
    if (frag === current) return;
    current = frag;
    render(frag, event.state);
    zombular.page.update();
    return false;
}

export default Object.assign(_=>page, {
    register(route, f) {
        routes[route] = f;
        render(getFragment());
        return this;
    },
    navigate(url, state={}) {
        history.pushState(state, null, url);
        window.onpopstate({});
    },
    page
});
