from autobahn.twisted import wamp
from twisted.internet.defer import inlineCallbacks

from collections import defaultdict

class Storage(wamp.ApplicationSession):
    """ Сессионное хранилище """
    def __init__(self, *args, **kwargs):
        wamp.ApplicationSession.__init__(self, *args, **kwargs)    
        self.sessions = defaultdict(dict)

    @inlineCallbacks
    def onJoin(self, details):
        yield self.subscribe(self.on_client_join, 'wamp.session.on_join')
        yield self.subscribe(self.on_client_leave, 'wamp.session.on_leave')

        yield self.register(self.set_, 'sessions._set')
        yield self.register(self.get, 'sessions._get')
        
    def on_client_join(self, details):
        client_id = details['session']
        self.sessions[client_id] = {}

    def on_client_leave(self, client_id):
        self.sessions.pop(client_id)

    def set_(self, session: str, key: str, value: str):
        """ Установить значение сессии """
        self.sessions[session][key] = value

    def get(self, session: str, key: str) -> str or None:
        """ Получить значение сессии """
        return self.sessions[session].get(key)
