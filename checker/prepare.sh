#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

apt update
apt install -y python3 python3-pip python3-virtualenv
cd $DIR
python3 -m venv env
source env/bin/activate
python3 -m pip install autobahn[asyncio,encryption,serialization,xbr] redis Faker

