from shared import *

class Component(ApplicationSession):
    async def onJoin(self, details):
        try:
            user = generate_user()
            await self.call('user.register', **user)
            driver = generate_driver()
            driver['car'] = 'CYB3RTRVKK'
            driver['tarriff_base'] = 313377451
            driver['is_private'] = False
            driver['is_premium'] = True
            driver['description'] = 'ЛУЧШЕ, ЧЕМ ГРУЗОВИК, С БОЛЬШЕЙ ПРОИЗВОДИТЕЛЬНОСТЬЮ, ЧЕМ СПОРТИВНЫЙ АВТОМОБИЛЬ'
            await self.call('driver.register', driver)
            redis.set(f'checkers_state/taxi/{IP}/username', user['name'])
            redis.set(f'checkers_state/taxi/{IP}/password', user['password'])
        except Exception as e:
            trace.write('\n'+str(e))
            trace.state = MUMBLE
            trace.error(e)

        self.leave()

    def onDisconnect(self):
        asyncio.get_event_loop().stop()

if __name__ == '__main__':
    try:
        runner.run(Component)
    except Exception as e:
        trace.state = DOWN
        trace.write('Не удалось подключиться')
        trace.error(e)
    trace.print()
