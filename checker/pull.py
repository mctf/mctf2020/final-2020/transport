from shared import *

class Component(ApplicationSession):
    async def onJoin(self, details):
        try:
            name = redis.get(f'checkers_state/taxi/{IP}/username')
            password = redis.get(f'checkers_state/taxi/{IP}/password')
            await self.call('user.login', name, password)
            profile = await self.call('profile.get')
            driver = await self.call('driver.get')
            try:
                assert profile['chip'] in FLAG
                assert driver['prepaid_comment'] in FLAG
            except:
                trace.tb += [f'{profile}\n{driver}\n']
                trace.write('Невозможно получить флаг')
                trace.state = CORRUPTED
        except Exception as e:
            trace.write('\n'+str(e))
            trace.state = MUMBLE
            trace.error(e)

        self.leave()

    def onDisconnect(self):
        asyncio.get_event_loop().stop()

if __name__ == '__main__':
    try:
        runner.run(Component)
    except Exception as e:
        trace.state = DOWN
        trace.write('Не удалось подключиться')
        trace.error(e)
    trace.print()
